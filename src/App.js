import Login from "./componentes/login/Login";
import { GlobalStyle } from "./componentes/style";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Alumno from "./componentes/alumnos/Alumno";

function App() {
  return (
    <div className='App'>
      <GlobalStyle />
      <BrowserRouter>
          <Routes>
            <Route exact path="/" element={ <Login /> } />
            <Route exact path="alumno/:nombre" element={ <Alumno /> } />
          </Routes>
      </BrowserRouter>
    </div>

  );
}

export default App;
