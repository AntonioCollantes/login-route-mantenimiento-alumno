import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
html{
  height: 100%;
}

*,*::after, ::before{
  margin: 0;
  padding: 0;
  box-sizing: inherit;
}

body{
    margin: 0;
    padding: 0;
    background-color: #17a2b8;
    height: 100vh;
}
`;