import { Form, StyleInput, Button, BannerDiv, Container } from './style';
import React, { Component } from 'react';
import { useNavigate } from 'react-router-dom';
//import { createSearchParams } from 'react-router-dom';

const initialValues = {
    usuario: "",
    clave: ""
}

let habilitar = true;

///AGREGA PARAMETROS A LA URL ejm: /nuse=antonio
/**const useNavigateAlumno = () => {
    const navigate = useNavigate();
    return (pathname, params) =>
        navigate({ pathname, search: `?${createSearchParams(params)}` });
};

const Alumnos = (props) => {
    console.log("props " + props.usu.usuario)
    const navigateAlumno = useNavigateAlumno();
    const nomUsuario = props.usu.usuario
    const goToAlumn = () =>
        navigateAlumno('/alumno', { nUse: nomUsuario });

    return (
        <div>
            <Button onClick={goToAlumn} disabled={habilitar}>Login</Button>
        </div>
    );
};*/

const Alumnos = (props) => {
    let navigate = useNavigate();
    const usuario = props.usu.usuario;
    function handleClick() {
        navigate(`alumno/${usuario}`);
    }

    return (
        <div>
            <Button onClick={handleClick} disabled={habilitar}>Login</Button>
        </div>
    );
}

class Login extends Component {

    constructor() {
        super();
        this.state = { ...initialValues }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        }
        );

        const { usuario, clave } = this.state;
        if (usuario.length > 0 && clave.length > 0) {
            habilitar = false;
        }

    }

    render() {
        return (
            <Container>
                <BannerDiv />
                <div className="row justify-content-center align-items-center">
                    <div className="col-md-6">
                        <div className="col-md-12">
                            <Form>
                                <h3 className="text-center text-info">Login</h3>
                                <div className="form-group">
                                    <label htmlFor="username" className="text-info">Username:</label><br />
                                    <StyleInput
                                        type='text'
                                        name='usuario'
                                        placeholder='Ingresar E-mail'
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password" className="text-info">Password:</label><br />
                                    <StyleInput
                                        type='password'
                                        name='clave'
                                        placeholder='Ingresar Contraseña'
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <br />
                                <div className="form-group">
                                    {/* <Alumnos usu={this.state} />*/}
                                    <Alumnos usu={this.state} />
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </Container>
        );
    }
}

export default Login;