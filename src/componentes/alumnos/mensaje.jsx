import Swal from 'sweetalert2'

const mensaje = (msg, icono) => {
    Swal.fire({
        position: 'top',
        icon: icono,
        title: msg,
        showConfirmButton: false,
        timer: 3500
      })
}

export default mensaje;