import styled, {css} from 'styled-components';

export const Form = styled.form`
  padding: 20px;
`;

const sharedInputStyled = css`
  display: block;
  width: 100%;
  background-color: #675235;
  border-radius: 5px;
  border: 1px solid #ddd;
  margin: 10px 0 20px 0;
  padding: 20px;
  box-sizing: border-box;
`;

export const StyleInput = styled.input`
  &[type="password"] {
    border: 1px solid black;
    background: red;
  }
  height: 40px;
  ${sharedInputStyled}
`;

export const Button = styled.button`
  background-color: red;
  color: white;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid black;
  border-radius: 2px;
`;

export const FormGroup = styled.div`
  flex: 0 0 ${(props) => (props.col === '1' ? 100 : 100 / props.col - 2)}%;
  @media (max-width: 900px) {
    flex: 0 0 100%;
  }
`;

export const ButtonTbl = styled.button`
  background-color: black;
  color: white;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
`;

export const BannerDiv = styled.div`
  color: white;
  background-image: url("https://www.mediasource.mx/hubfs/Atrae-alumnos-y-aumenta-las-inscripciones-al-100-de-tu-capacidad.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 30vh;
`;

const sharedStyled = css`
    margin-top: 10px;
    max-width: 900px;
    height: 280;
    border: 1px solid #9C9C9C;
    background-color: #EAEAEA;
`;

export const Container = styled.div`
  max-width: 1140px;
  background-color: #f6f6f6;
  margin: 0 auto;
  ${sharedStyled}
`;