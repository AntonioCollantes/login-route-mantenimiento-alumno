import React, { Component } from 'react';
import { Form, FormGroup, StyleInput, Button } from './style';
import { v4 as generaID } from 'uuid';
import mensaje from './mensaje';
import FrmTableAlumno from './FrmTableAlumno';

const initState = {
    nombre: '',
    apellido: '',
    email: '',
    celular: '',
    fechaNaci: ''
}

let datos = [];
let idBuscarAlumno = '';

class FormAlumno extends Component {

    constructor() {
        super();
        this.state = { ...initState }
    }

    getText = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    verificaCampoVacios = () => {
        let validacion = true;
        const { nombre, apellido, email, celular, fechaNaci } = this.state;
        if (nombre === '' || apellido === '' || email === '' || celular === '' || fechaNaci === '') {
            return false;
        }
        return validacion;
    }

    enviarDatos = (event) => {
        event.preventDefault();
        console.log("idBuscarAlumno en guardar" + idBuscarAlumno);
        if (idBuscarAlumno === '') {
            let datosAlumnos = {};
            if (this.verificaCampoVacios()) {
                const id = generaID();
                datosAlumnos = { id, ...this.state };
                datos.push(datosAlumnos);
                this.setState({ ...initState });
                mensaje("Se Registro Alumno Correctamente", 'success');
            } else {
                mensaje("Ingrese todos los campos Obligatorios", 'warning');
            }
        } else {
            let indice = datos.findIndex(datos => datos.id === idBuscarAlumno);
            console.log("indice " + indice);
            let id = idBuscarAlumno;
            datos[indice] = { id, ...this.state };
            console.log("Lista de Alumnos " + JSON.stringify(datos));
            idBuscarAlumno = '';
            this.setState({ ...initState });
            mensaje("Se Guardaron Cambios de Alumno Correctamente", 'success');
        }

    }

    eliminarAlumno = (idAlumno) => {
        console.log(" Id Eliminar " + idAlumno);
        datos = datos.filter(
            datos => datos.id !== idAlumno
        );
        mensaje("Alumno Eliminado Exitosamente", 'success');
        console.log("Lista de Alumnos " + JSON.stringify(datos))
        this.setState({ ...initState });
    }

    editarAlumno = (idAlumno) => {
        console.log(" Id idAlumno " + idAlumno);
        let dato = datos.find(datos => datos.id === idAlumno);
        const { id, nombre, apellido, email, celular, fechaNaci } = dato;
        this.setState({
            nombre: nombre,
            apellido: apellido,
            email: email,
            celular: celular,
            fechaNaci: fechaNaci
        });
        idBuscarAlumno = id;
        console.log(" Id idBuscarAlumno " + idBuscarAlumno);
        console.log(" Alumno " + JSON.stringify(dato));
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.enviarDatos}>

                    <legend className="text-center header">Mantenimiento de Alumnos</legend>

                    <FormGroup col='2'>
                        <div className="col-md-8">
                            <label>Nombres</label>
                            <StyleInput
                                name="nombre"
                                type="text"
                                placeholder="Nombres"
                                onChange={this.getText}
                                value={this.state.nombre}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup col='2'>
                        <div className="col-md-8">
                            <label>Apellidos</label>
                            <StyleInput
                                name="apellido"
                                type="text"
                                placeholder="Apellidos"
                                onChange={this.getText}
                                value={this.state.apellido}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup col='2'>
                        <div className="col-md-8">
                            <label>Email</label>
                            <StyleInput
                                name="email"
                                type="text"
                                placeholder="Email"
                                onChange={this.getText}
                                value={this.state.email}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup col='2'>
                        <div className="col-md-8">
                            <label>Celular</label>
                            <StyleInput
                                name="celular"
                                type="text"
                                placeholder="Celular"
                                onChange={this.getText}
                                value={this.state.celular}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup col='2'>
                        <div className="col-md-8">
                            <label>Fecha Nacimiento</label>
                            <StyleInput
                                name="fechaNaci"
                                type="date"
                                placeholder="Fecha nacimiento"
                                onChange={this.getText}
                                value={this.state.fechaNaci}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup col='2'>
                        <div className="col-md-12 text-center">
                            <Button type='submit'>Registrar</Button>
                        </div>
                    </FormGroup>
                </Form>
                <FrmTableAlumno
                    listaAlumnos={datos}
                    eliminarAlumno={this.eliminarAlumno}
                    editarAlumno={this.editarAlumno}
                />
            </div>
        );
    }
}

export default FormAlumno;