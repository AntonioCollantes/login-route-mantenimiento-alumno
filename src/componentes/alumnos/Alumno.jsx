import React from 'react';
import { useParams } from "react-router-dom";
import { BannerDiv, Container } from './style';
import FormAlumno from './FormAlumno';
import TituloAlumn from './TituloAlumn';

function Alumno() {

    const { nombre } = useParams();
    const msgTitulo = `Bienvenido Al Sistema ${nombre}`;

    return (
        <Container>
            <TituloAlumn titulo={msgTitulo} />
            <BannerDiv />
            <FormAlumno />
        </Container>
    );
}

export default Alumno;