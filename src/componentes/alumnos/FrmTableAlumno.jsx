import React, { Component } from 'react';
import { ButtonTbl } from './style';

class FrmTableAlumno extends Component {

    render() {
        const { listaAlumnos, eliminarAlumno, editarAlumno } = this.props;
        return (
            <div>
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Nombres</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Email</th>
                            <th scope="col">Celular</th>
                            <th scope="col">F. Nacimiento</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listaAlumnos.map((alumno, index) => {
                            return (
                                <tr key={index}>
                                    <td>{alumno.nombre}</td>
                                    <td>{alumno.apellido}</td>
                                    <td>{alumno.email}</td>
                                    <td>{alumno.celular}</td>
                                    <td>{alumno.fechaNaci}</td>
                                    <td>
                                        <ButtonTbl onClick = {() => editarAlumno(alumno.id)}>Editar</ButtonTbl>
                                    </td>
                                    <td>
                                        <ButtonTbl onClick = {() => eliminarAlumno(alumno.id)}>Eliminar</ButtonTbl>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default FrmTableAlumno;