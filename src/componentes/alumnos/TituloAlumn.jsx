import React from 'react';
import styled from 'styled-components';

const Title = styled.h2`
  color: #7271fb;
  text-align: center;
  margin: 0;
`;

const TituloAlumn = ({ titulo }) => {
    return <Title> { titulo } </Title>;
};

export default TituloAlumn;